# this script will speed up initial subdomain lookup
echo "# domain resolution for aisco subdomain: ###$1" | sudo tee --append /etc/hosts
echo "127.0.0.1	$1" | sudo tee --append /etc/hosts
