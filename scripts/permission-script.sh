# auth.properties # used when building a product (credentials to login into generated product)
sudo chmod g+rwx auth.properties
sudo chgrp www-data auth.properties
# config.properties # used when building a product (db credential for generated products)
sudo chmod g+rwx config.properties
sudo chgrp www-data config.properties
# generated_products/ # used when building a product (storing generated product files)
sudo chmod -R g+rwx generated_products/
sudo chgrp -R www-data generated_products/
# src/java/ # used when building a product (put generated fli)
sudo chmod -R g+rwx src/java
sudo chgrp -R www-data src/java
# target/ # used when building a product (put intermediate file)
sudo chmod -R g+rwx target/ external/ lib/
sudo chgrp -R www-data target/ external/ lib/
# src/abs/product/ # used when building a product (create Product.abs)
sudo chmod -R g+rwx src/abs/product/
sudo chgrp -R www-data src/abs/product/
# src/abs/model/ # used when building a product (put generated repository code <modelDb>)
sudo chmod -R g+rwx src/abs/model/
sudo chgrp -R www-data src/abs/model/
# lib/absdborm.jar # used when building a product (put library to generated product)
sudo chmod g+rwx lib/absdborm.jar
sudo chgrp www-data lib/absdborm.jar
# featuregroupingpagev2 # to access product building page
sudo chgrp www-data featuregroupingpagev2/
sudo find featuregroupingpagev2/ -type d -exec chmod 775 {} \;    
sudo find featuregroupingpagev2/ -type f -exec chmod 664 {} \;    
# project root
sudo chmod g+rwx .
sudo chgrp www-data .
# env
sudo chmod g+rwx .env
sudo chgrp www-data .env
# builder build.sh
sudo chmod g+rwx build.sh run.sh build.xml
sudo chgrp www-data build.sh run.sh build.xml
# src/ # accessed by build.xml, it need to build all abs (including feature model, product line dll)
sudo chmod -R g+rwx src/
sudo chgrp -R www-data src/
# scripts
sudo chmod g+rwx scripts/stop-instance.sh scripts/reload-apache.sh scripts/remove-intermediary-files.sh
sudo chgrp www-data scripts/stop-instance.sh scripts/reload-apache.sh scripts/remove-intermediary-files.sh
# sudo find . -type d -exec chmod 775 {} \;    
# sudo find . -type f -exec chmod 664 {} \;    
