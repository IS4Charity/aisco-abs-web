
# set default product if it not supplied
if [ "$#" -lt 1 ]; then
	productname=Minimal
else
	productname=$1
fi


echo "WARNING: EXECUTION ASSUMED TO BE ON PROJECT ROOT"

# check "config.properties" and "auth.properties"
if [ ! -f config.properties ]; then
    echo "please copy .config.properties.example into config.properties"
    exit
fi
if [ ! -f auth.properties ]; then
    echo "please copy .auth.properties.example into auto.properties"
    exit
fi




# prepare the folders
mkdir target
mkdir generated_products
mkdir "generated_products/$productname"
mkdir -p "src/java/fli"
log_path="generated_products/"$productname"/build_output"

echo "[IN CASE BUILD FAIL, TRY TO ADD -VERBOSE / -DEBUG ON ANT SCRIPT]"
if [ "$#" -eq 0 ]; then
    echo "first parameter is empty, product name default to '$productname'"
    # abs.deploy is the first stage in build.xml
    ant -Dabsproduct="$productname" abs.deploy > $log_path 2> $log_path
else
    productname=$1
    ant -Dabsproduct="$productname" abs.deploy > $log_path 2> $log_path
fi

if [ -f generated_products/"$productname"/external ]; then
    rm -r generated_products/"$productname"/external
fi
cp -r external generated_products/"$productname"/external
cp -r lib generated_products/"$productname"/lib
#cp -r config.properties generated_products/"$productname"/config.properties
#cp -r auth.properties generated_products/"$productname"/auth.properties

cat $log_path


if [ ! -z "$(cat $log_path | grep 'BUILD SUCCESSFUL')" ]; then
    echo "build done!(exit code $?) generated web at generated_products/$productname/"
elif [ ! -z "$(cat $log_path | grep 'BUILD FAILED')" ]; then
    echo "build failed !!, removing remainings"
    mv "src/abs/product/$productname.abs" "generated_products/$productname/$productname.abs"
    # virtualhosts defined at featuregroupingpagev2/download.php
    bash scripts/stop-instance.sh "$productname"
else
    echo "build failed !!, removing remainings, undefined state"
    mv "src/abs/product/$productname.abs" "generated_products/$productname/$productname.abs"
    # virtualhosts defined at featuregroupingpagev2/download.php
    bash scripts/stop-instance.sh "$productname"
fi

