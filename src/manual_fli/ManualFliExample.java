// Name this file "ManualFLIExample.java" will be ignored by the build process
package ABS.Framework.Hello;
import abs.backend.java.lib.types.ABSString;
import abs.backend.java.lib.types.ABSInteger;
import abs.backend.java.lib.types.ABSUnit;
import abs.backend.java.lib.runtime.FLIHelper;
import java.lang.reflect.Method; 

public class HelloImpl_fli extends HelloImpl_c { 
    @Override
    public ABSString fli_hello(ABSString msg) { 
        FLIHelper.println("I got "+msg.getString()+" from ABS");
        System.out.println("hei hooooo");
        return ABSString.fromString("Hello ABS, this is Java");
    } 
    @Override
    public ABSUnit fli_echoStr(ABSString msg) { 
        System.out.println(msg.getString());
        return abs.backend.java.lib.types.ABSUnit.UNIT;
    } 
    @Override
    public ABSUnit fli_echoInt(ABSInteger msg) { 
        System.out.println(msg.toInt());
        return abs.backend.java.lib.types.ABSUnit.UNIT;
    } 
}

