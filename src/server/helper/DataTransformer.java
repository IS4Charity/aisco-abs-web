package com.fmse.absserver.helper;

import java.util.ArrayList;
import java.util.HashMap; 


import ABS.StdLib.Pair_Pair;
import abs.backend.java.lib.runtime.ABSObject;
import abs.backend.java.lib.types.ABSString;
import abs.backend.java.lib.types.ABSInteger;
import abs.backend.java.lib.types.ABSValue;

/*
 * ACKNOWLADGE: https://github.com/sir-muamua/ABSServer/blob/master/src/com/fmse/absserver/helper/DataTransformer.java
 */
public class DataTransformer {

    public static ArrayList<Object>
            convertABSListToJavaList(ABS.StdLib.List<ABSValue> target) throws Exception {
        ArrayList<Object> result = new ArrayList<Object>();

        do {
            ABSObject head = (ABSObject) ABS.StdLib.head_f.apply(target);
            result.add(head);

            target = ABS.StdLib.tail_f.apply(target);
        } while (!(target instanceof ABS.StdLib.List_Nil));

        return result;
    }

    
    public static HashMap<String, Integer>
            convertViewModelToJava(ABS.StdLib.List<ABSValue> target) throws Exception {
        HashMap<String, Integer> result = new HashMap<>();

        do {
            Pair_Pair head = (Pair_Pair)ABS.StdLib.head_f.apply(target);
            String temp1 = ((ABSString)head.getArg(0)).getString();
            Integer temp2 = ((ABSInteger)head.getArg(1)).toInt();
            result.put(temp1, temp2); 

            target = ABS.StdLib.tail_f.apply(target);
        } while (!(target instanceof ABS.StdLib.List_Nil));

        return result;
    }


    public static String
            convertABSStringToJavaString(ABSString target) throws Exception {
        return target.toString().replaceAll("\"", "");
    }
}
