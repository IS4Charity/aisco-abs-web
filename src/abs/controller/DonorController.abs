module MDonorController;
import Donor, DonorImpl from MDonorModel;
import DonorDb, DonorDbImpl from MDonorDbImpl;
import ABSHttpRequest from ABS.Framework.Http;
import Utility, UtilityImpl from ABS.Framework.Utility;
import ViewHelperController, ViewHelperControllerImpl from MViewHelperController;

interface DonorController
{
    Triple<String, List<Donor>, List<Pair<String,Int>>> list(ABSHttpRequest request);
    Triple<String, List<Donor>, List<Pair<String,Int>>> detail(ABSHttpRequest request);
    Triple<String, List<Donor>, List<Pair<String,Int>>> create(ABSHttpRequest request);
    Triple<String, List<Donor>, List<Pair<String,Int>>> save(ABSHttpRequest request);
    Triple<String, List<Donor>, List<Pair<String,Int>>> edit(ABSHttpRequest request);
    Triple<String, List<Donor>, List<Pair<String,Int>>> update(ABSHttpRequest request);
    Triple<String, List<Donor>, List<Pair<String,Int>>> delete(ABSHttpRequest request);
}

class DonorControllerImpl implements DonorController
{
    Triple<String, List<Donor>, List<Pair<String,Int>>> list(ABSHttpRequest request) {
        DonorDb orm = new local DonorDbImpl();

        List<Donor> donors = orm.findAll("MDonorModel.DonorImpl_c");

        ViewHelperController v = new local ViewHelperControllerImpl();
        List<Pair<String,Int>> viewModel = v.getViewModel();
        return Triple("donor/list", donors, viewModel);
    }

    Triple<String, List<Donor>, List<Pair<String,Int>>> detail(ABSHttpRequest request) {
        String id = request.getInput("idDonor");
        String condition = "idDonor=" + id;
        DonorDb orm = new local DonorDbImpl();
        Donor donor = orm.findByAttributes("MDonorModel.DonorImpl_c",condition);

        List<Donor> dataModel = Nil;
        dataModel = appendright(dataModel, donor);

        ViewHelperController v = new local ViewHelperControllerImpl();
        List<Pair<String,Int>> viewModel = v.getViewModel();
        return Triple("donor/detail", dataModel, viewModel);
    }

    Triple<String, List<Donor>, List<Pair<String,Int>>> create(ABSHttpRequest request) {
        ViewHelperController v = new local ViewHelperControllerImpl();
        List<Pair<String,Int>> viewModel = v.getViewModel();
        return Triple("donor/create", Nil, viewModel);
    }

    Triple<String, List<Donor>, List<Pair<String,Int>>> save(ABSHttpRequest request) {
        Utility utility = new local UtilityImpl();

        DonorDb orm = new local DonorDbImpl();
        Donor donor = new local DonorImpl();

        String name = request.getInput("name");
        String email = request.getInput("email");
        String phone = request.getInput("phone");
        String address = request.getInput("address");

        donor.setName(name);
        donor.setEmail(email);
        donor.setPhone(phone);
        donor.setAddress(address);
        orm.save(donor);

        ViewHelperController v = new local ViewHelperControllerImpl();
        List<Pair<String,Int>> viewModel = v.getViewModel();
        return Triple("redirect:admin/donor/list.abs", Nil, viewModel);
    }

    Triple<String, List<Donor>, List<Pair<String,Int>>> edit(ABSHttpRequest request) {
        String id = request.getInput("idDonor");
        String condition = "idDonor=" + id;
        DonorDb orm = new local DonorDbImpl();
        Donor donor = orm.findByAttributes("MDonorModel.DonorImpl_c", condition);

        List<Donor> dataModel = Nil;
        dataModel = appendright(dataModel, donor);
        ViewHelperController v = new local ViewHelperControllerImpl();
        List<Pair<String,Int>> viewModel = v.getViewModel();
        return Triple("donor/edit", dataModel, viewModel);
    }

    Triple<String, List<Donor>, List<Pair<String,Int>>> update(ABSHttpRequest request) {
        Utility utility = new local UtilityImpl();

        String id = request.getInput("idDonor");
        String condition = "idDonor=" + id;
        DonorDb orm = new local DonorDbImpl();
        Donor donor = orm.findByAttributes("MDonorModel.DonorImpl_c",condition);

        String name = request.getInput("name");
        String email = request.getInput("email");
        String phone = request.getInput("phone");
        String address = request.getInput("address");

        donor.setName(name);
        donor.setEmail(email);
        donor.setPhone(phone);
        donor.setAddress(address);
        orm.update(donor);

        return this.detail(request);
    }

    Triple<String, List<Donor>, List<Pair<String,Int>>> delete(ABSHttpRequest request) {
        String id = request.getInput("idDonor");
        String condition = "idDonor=" + id;
        DonorDb orm = new local DonorDbImpl();
        Donor donor = orm.findByAttributes("MDonorModel.DonorImpl_c", condition);

        orm.delete(donor);

        ViewHelperController v = new local ViewHelperControllerImpl();
        List<Pair<String,Int>> viewModel = v.getViewModel();
        return Triple("redirect:admin/donor/list.abs", Nil, viewModel);
    }
}

