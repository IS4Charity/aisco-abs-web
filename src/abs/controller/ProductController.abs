module MProductController;
import Product, ProductImpl from MProductModel;
import ProductDb, ProductDbImpl from MProductDbImpl;
import ABSHttpRequest from ABS.Framework.Http;
import Utility, UtilityImpl from ABS.Framework.Utility;
import ViewHelperController, ViewHelperControllerImpl from MViewHelperController;

interface ProductController
{
    Triple<String, List<Product>, List<Pair<String,Int>>> list(ABSHttpRequest request);
    Triple<String, List<Product>, List<Pair<String,Int>>> detail(ABSHttpRequest request);
    Triple<String, List<Product>, List<Pair<String,Int>>> create(ABSHttpRequest request);
    Triple<String, List<Product>, List<Pair<String,Int>>> save(ABSHttpRequest request);
    Triple<String, List<Product>, List<Pair<String,Int>>> edit(ABSHttpRequest request);
    Triple<String, List<Product>, List<Pair<String,Int>>> update(ABSHttpRequest request);
    Triple<String, List<Product>, List<Pair<String,Int>>> delete(ABSHttpRequest request);
}

class ProductControllerImpl implements ProductController
{
    Triple<String, List<Product>, List<Pair<String,Int>>> list(ABSHttpRequest request) {
        ProductDb orm = new local ProductDbImpl();

        List<Product> productItems = orm.findAll("MProductModel.ProductImpl_c");

        ViewHelperController v = new local ViewHelperControllerImpl();
        List<Pair<String,Int>> viewModel = v.getViewModel();
        return Triple("product/list", productItems, viewModel);
    }

    Triple<String, List<Product>, List<Pair<String,Int>>> detail(ABSHttpRequest request) {
        String id = request.getInput("idProduct");
        String condition = "idProduct=" + id;
        ProductDb orm = new local ProductDbImpl();
        Product productItem = orm.findByAttributes("MProductModel.ProductImpl_c",condition);

        List<Product> dataModel = Nil;
        dataModel = appendright(dataModel, productItem);

        ViewHelperController v = new local ViewHelperControllerImpl();
        List<Pair<String,Int>> viewModel = v.getViewModel();
        return Triple("product/detail", dataModel, viewModel);
    }

    Triple<String, List<Product>, List<Pair<String,Int>>> create(ABSHttpRequest request) {
        ViewHelperController v = new local ViewHelperControllerImpl();
        List<Pair<String,Int>> viewModel = v.getViewModel();
        return Triple("product/create", Nil, viewModel);
    }

    Triple<String, List<Product>, List<Pair<String,Int>>> save(ABSHttpRequest request) {
        Utility utility = new local UtilityImpl();

        ProductDb orm = new local ProductDbImpl();
        Product productItem = new local ProductImpl();

        String name = request.getInput("name");
        String description = request.getInput("description");

        productItem.setName(name);
        productItem.setDescription(description);
        orm.save(productItem);

        ViewHelperController v = new local ViewHelperControllerImpl();
        List<Pair<String,Int>> viewModel = v.getViewModel();
        return Triple("redirect:admin/product/list.abs", Nil, viewModel);
    }

    Triple<String, List<Product>, List<Pair<String,Int>>> edit(ABSHttpRequest request) {
        String id = request.getInput("idProduct");
        String condition = "idProduct=" + id;
        ProductDb orm = new local ProductDbImpl();
        Product productItem = orm.findByAttributes("MProductModel.ProductImpl_c", condition);

        List<Product> dataModel = Nil;
        dataModel = appendright(dataModel, productItem);
        ViewHelperController v = new local ViewHelperControllerImpl();
        List<Pair<String,Int>> viewModel = v.getViewModel();
        return Triple("product/edit", dataModel, viewModel);
    }

    Triple<String, List<Product>, List<Pair<String,Int>>> update(ABSHttpRequest request) {
        Utility utility = new local UtilityImpl();

        String id = request.getInput("idProduct");
        String condition = "idProduct=" + id;
        ProductDb orm = new local ProductDbImpl();
        Product productItem = orm.findByAttributes("MProductModel.ProductImpl_c",condition);

        String name = request.getInput("name");
        String description = request.getInput("description");

        productItem.setName(name);
        productItem.setDescription(description);
        orm.update(productItem);

        return this.detail(request);
    }

    Triple<String, List<Product>, List<Pair<String,Int>>> delete(ABSHttpRequest request) {
        String id = request.getInput("idProduct");
        String condition = "idProduct=" + id;
        ProductDb orm = new local ProductDbImpl();
        Product productItem = orm.findByAttributes("MProductModel.ProductImpl_c", condition);

        orm.delete(productItem);

        ViewHelperController v = new local ViewHelperControllerImpl();
        List<Pair<String,Int>> viewModel = v.getViewModel();
        return Triple("redirect:admin/product/list.abs", Nil, viewModel);
    }
}

