echo "warning: execution assumed to be on project root"

# get productname, and port. set default if not exists
if [ "$#" -lt 1 ]; then
    echo "usage: bash run.sh [ProductName]"
    echo "parameter is less than 1, product name to '$productname'"
    productname=Minimal
    port=9027
else
    productname=$1
    productnamelower=$(echo "$productname" | tr '[:upper:]' '[:lower:]')
    port=$(cat virtual_hosts/$productnamelower.conf | grep -i proxypassreverse | cut -d':' -f3 | cut -d'/' -f1)
    echo "using port : $port for $productname"
fi

cd generated_products/$productname
java -jar "$productname.jar" -p $port
