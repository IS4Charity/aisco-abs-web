# Release Note
## v0.2.0
### Added 
- Chart of Account
- Role Access (Static page)

## v0.1.0
### Added
    - Framwork skeleton (Merge from several old repositories)
    - Combine AISCO Implementation from several old repositories
    - Dockerize project
    - Reformat branch name, follow convention
    - Bug fixing
    - Enable to return view model from controller
    - COA
    - Delta-ed View
    - Manual FLI
    - Getting started wiki
### Updated
    - Build script
    - Run script
    - Folder structure
    - Default product front page
    - Refactor all controller to use view model
### Fixed
    - Product selection page handler
    - Broken route
    - UI Styling
### Removed
### Hotfixed
