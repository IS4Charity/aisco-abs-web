DROP TABLE IF EXISTS `product`;
CREATE TABLE product (
  `org_name` varchar(200) DEFAULT NULL,
  `admin_name` varchar(200) DEFAULT NULL,
  `admin_email` varchar(200) DEFAULT NULL,
  `admin_password` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `db_name` varchar(200) DEFAULT NULL,
  `port` varchar(200) DEFAULT NULL,
  `is_running` tinyint NOT NULL DEFAULT '0',
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
)  ;

