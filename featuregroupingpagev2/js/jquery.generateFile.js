(function($){

  // Creating a jQuery plugin:

  $.generateFile = function(options){

    options = options || {};

    if(!options.script || !options.filename || !options.content){
      throw new Error("Please enter all the required config options!");
    }

    // Creating a 1 by 1 px invisible iframe:

    var iframe = $('<iframe>',{
      width:1,
      height:1,
      frameborder:0,
      css:{
        display:'none'
      }
    }).appendTo('body');

    var formHTML = '<form action="" method="post">'+
      '<input type="hidden" name="filename" />'+
      '<input type="hidden" name="content" />'+
      '<input type="hidden" name="orgname" />'+
      '<input type="hidden" name="dbname" />'+
      '<input type="hidden" name="adminname" />'+
      '<input type="hidden" name="adminemail" />'+
      '</form>';

    // Giving IE a chance to build the DOM in
    // the iframe with a short timeout:

    setTimeout(function(){

      // The body element of the iframe document:

      var body = (iframe.prop('contentDocument') !== undefined) ?
        iframe.prop('contentDocument').body :
        iframe.prop('document').body;	// IE

      body = $(body);

      // Adding the form to the body:
      body.html(formHTML);

      // var form = body.find('form');
      // form.attr('action',options.script);
      // form.find('input[name=filename]').val(options.filename);
      // form.find('input[name=content]').val(options.content);
      // form.find('input[name=orgname]').val(options.orgname);
      // form.find('input[name=dbname]').val(options.dbname);
      // form.find('input[name=adminname]').val(options.adminname);
      // form.find('input[name=adminemail]').val(options.adminemail);

      // Submitting the form to download.php. This will
      // cause the file download dialog box to appear.

      // form.submit();

      // below use ajax
      console.log('request sent, please wait...')
      $.ajax({
        url: options.script,
        type: 'POST',
        dataType: 'json',
        ContentType: 'application/json',
        success : function (response) {
          if(response.status_code == 200){
            alert("program sucessfully built")
          }else if(response.status_code == 1){
            alert("Constraint failed, please contact web administrator")
          }else{
            alert("something is wrong, please contact web administrator")
          }
        },
        data: {
          productname: options.productname,
          filename: options.filename,
          content: options.content,
          orgname: options.orgname,
          dbname: options.dbname,
          adminname: options.adminname,
          adminemail: options.adminemail,
        } ,
      })

      alert('Request submitted. You can leave or wait in this page. We will notify by email if it is done');
    },50);
  };

})(jQuery);
