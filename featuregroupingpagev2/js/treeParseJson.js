var treeData = null;
var selFeatures = [];
var onCreateSelected = "";
var theRoot = null;

// file json(contain feature model) of this page
var jsonName = $('.jsonName').text();


// load already checked checkbox into "selFeatures"
// -- flatten the json
// -- find entry that has dynatree-selected and has no children
// -- put that into selFeatures


// load json into the checkbox tree
$.ajax(jsonName).done(function(result){
  treeData = result;
  $(function(){
    $("#groupTree").dynatree({
      checkbox: true,
      selectMode: 3,
      children: treeData,
      onCreate: function(node, nodeSpan) {
        theRoot = node.tree.getRoot();
      },
      onPostInit: function(isReloading, isError){
        var selected = theRoot.tree.getSelectedNodes();

        for (var i = 0; i < selected.length; i++) {
          var node = selected[i];

          if (node.isSelected() && node.data.children == null) {
            onCreateSelected = onCreateSelected + node.data.realName + ",";

            processSelected(onCreateSelected);
            checkIfSameThenSelect(node.data.realName, node.tree.getNotSelectedNodes());

            if (node.data.children != null) {
              var child = node.data.children;
              for (var i = 0; i < child.length; i++) {
                if (child[i].require) {
                  var nodeRequires = child[i].require;
                  var nodeRequiresArray = nodeRequires.split(",");
                  for (var y = 0; y < nodeRequiresArray.length; y++) {
                    var selectedNodes = node.tree.getSelectedNodes();
                    var notSelectedNodes = node.tree.getNotSelectedNodes();
                    toggleSelectRequire(nodeRequiresArray[y], selectedNodes, notSelectedNodes);
                  };
                };
                if (child[i].exclude) {
                  var nodeExcludes = child[i].exclude;
                  var nodeExcludesArray = nodeExcludes.split(",");
                  for (var y = 0; y < nodeExcludesArray.length; y++) {
                    var selectedNodes = node.tree.getSelectedNodes();
                    var notSelectedNodes = node.tree.getNotSelectedNodes();
                    toggleSelectExclude(nodeExcludesArray[y], selectedNodes, notSelectedNodes);
                  };
                };
              };	
            }
            else{
              if (node.data.require) {
                var nodeRequires = node.data.require;
                var nodeRequiresArray = nodeRequires.split(",");
                for (var i = 0; i < nodeRequiresArray.length; i++) {
                  var selectedNodes = node.tree.getSelectedNodes();
                  var notSelectedNodes = node.tree.getNotSelectedNodes();
                  toggleSelectRequire(nodeRequiresArray[i], selectedNodes, notSelectedNodes);
                };
              };
              if (node.data.exclude) {
                var nodeExcludes = node.data.exclude;
                var nodeExcludesArray = nodeExcludes.split(",");
                for (var i = 0; i < nodeExcludesArray.length; i++) {
                  var selectedNodes = node.tree.getSelectedNodes();
                  var notSelectedNodes = node.tree.getNotSelectedNodes();
                  toggleSelectExclude(nodeExcludesArray[i], selectedNodes, notSelectedNodes);
                };
              };
            }	
          };
          if (node.data.exclude && node.isSelected() && node.data.children.length != 0) {
            var nodeExcludes = node.data.exclude;
            var nodeExcludesArray = nodeExcludes.split(",");
            for (var i = 0; i < nodeExcludesArray.length; i++) {
              var selectedNodes = node.tree.getSelectedNodes();
              var notSelectedNodes = node.tree.getNotSelectedNodes();
              toggleSelectExclude(nodeExcludesArray[i], selectedNodes, notSelectedNodes);
            };
          };
        };

        var allNodes = theRoot.tree.getSelectedNodes().concat(theRoot.tree.getNotSelectedNodes());
        renameFeatureTitle(allNodes);
      },
      onSelect: function(select, node) {
        var selRealNames = $.map(node.tree.getSelectedNodes(), function(node){return node.data.realName;});
        var allselected = selRealNames.join(",").toString();

        processSelected(allselected);
        if (select) {
          selectWithSameName(node, node.tree.getNotSelectedNodes());
        }
        else {
          deSelectWithSameName(node, node.tree.getSelectedNodes());
        }

        if (node.hasChildren()) {
          var child = node.data.children;
          var nodeTitle  = node.data.title;

          var allNodes = node.tree.getSelectedNodes().concat(node.tree.getNotSelectedNodes());
          var otherGroup = [];
          for (var i = 0; i < allNodes.length; i++) {
            if (allNodes[i].hasChildren() && allNodes[i].data.title != nodeTitle) {
              otherGroup.push(allNodes[i]);
            };
          };

          for (var i = 0; i < child.length; i++) {
            var theNode = node.tree.getNodeByKey(child[i].key);

            if (theNode.data.require && select) {
              var nodeRequires = theNode.data.require;
              var nodeRequiresArray = nodeRequires.split(",");
              for (var y = 0; y < nodeRequiresArray.length; y++) {
                var selectedNodes = node.tree.getSelectedNodes();
                var notSelectedNodes = node.tree.getNotSelectedNodes();
                toggleSelectRequire(nodeRequiresArray[y], selectedNodes, notSelectedNodes);
              };
            };
            if (theNode.data.exclude && select) {
              var nodeExcludes = theNode.data.exclude;
              var nodeExcludesArray = nodeExcludes.split(",");
              for (var y = 0; y < nodeExcludesArray.length; y++) {
                var selectedNodes = node.tree.getSelectedNodes();
                var notSelectedNodes = node.tree.getNotSelectedNodes();
                toggleSelectExclude(nodeExcludesArray[y], selectedNodes, notSelectedNodes);
              };
            };
            if (theNode.data.require && !select) {
              var nodeRequires = theNode.data.require;
              var nodeRequiresArray = nodeRequires.split(",");
              for (var y = 0; y < nodeRequiresArray.length; y++) {
                var allNodes = node.tree.getSelectedNodes().concat(node.tree.getNotSelectedNodes());
                removeUnselectableRequireParent(nodeRequiresArray[y], allNodes);
              }

            };
            if (theNode.data.exclude && !select) {
              var nodeExcludes = theNode.data.exclude;
              var nodeExcludesArray = nodeExcludes.split(",");
              for (var y = 0; y < nodeExcludesArray.length; y++) {
                var notSelectedNodes = node.tree.getNotSelectedNodes();
                removeUnselectableExclude(nodeExcludesArray[y], notSelectedNodes);
              }
            };

            for (var y = 0; y < otherGroup.length; y++) {
              toggleChild(theNode, otherGroup[y].getChildren(), select);
            };

          };

        }
        else {
          if (node.data.require && select) {
            var nodeRequires = node.data.require;
            var nodeRequiresArray = nodeRequires.split(",");
            for (var i = 0; i < nodeRequiresArray.length; i++) {
              var selectedNodes = node.tree.getSelectedNodes();
              var notSelectedNodes = node.tree.getNotSelectedNodes();
              toggleSelectRequire(nodeRequiresArray[i], selectedNodes, notSelectedNodes);
            };
          };
          if (node.data.exclude && select) {
            var nodeExcludes = node.data.exclude;
            var nodeExcludesArray = nodeExcludes.split(",");
            for (var i = 0; i < nodeExcludesArray.length; i++) {
              var selectedNodes = node.tree.getSelectedNodes();
              var notSelectedNodes = node.tree.getNotSelectedNodes();
              toggleSelectExclude(nodeExcludesArray[i], selectedNodes, notSelectedNodes);
            };
          };
          if (node.data.require && !select) {
            var nodeRequires = node.data.require;
            var nodeRequiresArray = nodeRequires.split(",");
            for (var i = 0; i < nodeRequiresArray.length; i++) {
              var selectedNodes = node.tree.getSelectedNodes();
              removeUnselectableRequire(nodeRequiresArray[i], selectedNodes);
            }
          };
          if (node.data.exclude && !select) {
            var nodeExcludes = node.data.exclude;
            var nodeExcludesArray = nodeExcludes.split(",");
            for (var i = 0; i < nodeExcludesArray.length; i++) {
              var notSelectedNodes = node.tree.getNotSelectedNodes();
              removeUnselectableExclude(nodeExcludesArray[i], notSelectedNodes);
            }
          };
        }

      }
    });
    // workaround
    theRoot.tree.visit(function(node){
      if(node.data.title === "Essential"){
        node.select();
        node.data.unselectable = true;
        node.li.className = "disabled-check-req";
        // return false; // stop traversal (if we are only interested in first match)
      }
    });
  });
});

$(document).ready(function(){
  checkForm();

  $('#download').click(function(e){
    console.log('generate clicked')
    var productName = $('[name=productName]').val();
    var productNameFirstUpper = firstToUpperCase(productName);
    var organizationName = $('[name=organizationName]').val();
    var adminName = $('[name=adminName]').val();
    var adminEmail = $('[name=adminEmail]').val();

    $.getJSON( "ProductNames.json", function( data ) {
      var exist = 0;
      $.each( data, function( key, val ) {
        $.each( val, function( key2, val2 ) {
          if (key2 == "productName") {
            if (productNameFirstUpper.toLowerCase() == val2.toLowerCase()) {
              exist = exist + 1;
            }
          }
        });
      });
      if (exist != 0) {
        $('.error-text').text('Product already exist');
        $('[name=productName]').closest('.form-group').addClass('has-error');
      }
      else if (productNameFirstUpper.indexOf(" ") != -1){
        $('.error-text').text('Product name cannot contains space');
        $('[name=productName]').closest('.form-group').addClass('has-error');
      }
      else{
        // get url of current without last part
        var url = document.URL, 
        shortUrl=url.substring(0,url.lastIndexOf("/"));
        $.generateFile({
          productname : productNameFirstUpper,
          filename	: productNameFirstUpper + '.abs',
          dbname		: productName.toLowerCase(),
          orgname		: organizationName,
          adminname	: adminName,
          adminemail	: adminEmail,
          content		: getProduct(),
          // todo: make more general so it could be executed from local
          script		: shortUrl+'/download.php'
        });
        checkForm();
      }
    });		
    e.preventDefault();
  });

  $(document).on('change keyup', '[name=productName]', function(){
    checkForm();
    $('.error-text').text('');
    $(this).closest('.form-group').removeClass('has-error');
  });
  $(document).on('change keyup', '[name=organizationName]', function(){
    checkForm();
  });
  $(document).on('change keyup', '[name=adminName]', function(){
    checkForm();
  });
  $(document).on('change keyup', '[name=adminEmail]', function(){
    checkForm();
  });

});

function checkForm(){
  var productName = $('[name=productName]').val();
  var organizationName = $('[name=organizationName]').val();
  var adminName = $('[name=adminName]').val();
  var adminEmail = $('[name=adminEmail]').val();

  if (productName == "" || organizationName == "" || adminName == "" || adminEmail == "") {
    $('#download').addClass('disabled');
    $('.error-text').text('Product name, organization name, admin name or admin email is empty');
  }
  else{
    $('#download').removeClass('disabled');
  }
}

function getProduct(){
  var features = getUnique(selFeatures);
  var res = "";
  for (var i = 0; i < features.length; i++) {
    if (i != features.length-1) {
      res += features[i] + ", ";
    }
    else{
      res += features[i];
    }
  };

  var information = "";
  information += "//----- Product information -----// \n";
  information += "// Organization Name: " + $('[name=organizationName]').val() + "\n";
  information += "// Admin Name: " + $('[name=adminName]').val() + "\n";
  information += "// Admin Email: " + $('[name=adminEmail]').val() + "\n";
  information += "//-------------------------------// \n";
  information += "\n";

  var productName = $('[name=productName]').val();
  var productNameFirstUpper = firstToUpperCase(productName);
  var result = information + "product " + productNameFirstUpper + "(" + res + ");";

  return result;
}

function getUnique(array){
  var unique = [];
  for (var i = 0; i < array.length; i++) {
    if (unique.indexOf(array[i]) == -1) {
      unique.push(array[i]);
    }
  }
  return unique;
};

function processSelected(featuresSelected){
  var keyArr = featuresSelected.split(",");
  var viewsArr = [];
  selFeatures = [];

  for(var i = 0; i < keyArr.length; i++) {
    var temp = keyArr[i].split("__");
    var fn = temp[temp.length-1];
    if (fn != "") {
      viewsArr.push('<span>' + fn + '</span><br>');
      selFeatures.push(keyArr[i]);
    };
  }

  var uniqueViewArr = getUnique(viewsArr).sort();
  var featureNames = uniqueViewArr.join("").toString();

  selFeatures.sort();
  fillHtmlDiv(featureNames, selFeatures);
  checkForm();
}

function fillHtmlDiv(selectedFeatureNames, selectedFeatures){
  document.getElementById("selectedchilds").innerHTML = selectedFeatureNames;
  document.getElementById("PSLexample").innerHTML = getProduct(selectedFeatures);		
};

function toggleSelectRequire(nodeRequire, selectedNodes, notSelectedNodes){
  for (var i = 0; i < notSelectedNodes.length; i++) {
    var currentNode = notSelectedNodes[i];
    var currentNodeRealName = currentNode.data.realName;
    if (currentNodeRealName == nodeRequire && !currentNode.isSelected()) {
      currentNode.toggleSelect();
      currentNode.data.unselectable = true;
      currentNode.li.className = "disabled-check-req";
    };
  };
  for (var i = 0; i < selectedNodes.length; i++) {
    var currentNode = selectedNodes[i];
    var currentNodeRealName = currentNode.data.realName;
    if (currentNodeRealName == nodeRequire && currentNode.isSelected()) {
      currentNode.data.unselectable = true;
      currentNode.li.className = "disabled-check-req";
    };
  };
}

function toggleSelectExclude(nodeExclude, selectedNodes, notSelectedNodes){
  for (var i = 0; i < selectedNodes.length; i++) {
    var currentNode = selectedNodes[i];
    var currentNodeRealName = currentNode.data.realName;
    if (currentNodeRealName == nodeExclude && currentNode.isSelected()) {
      currentNode.toggleSelect();
      currentNode.data.unselectable = true;
      currentNode.li.className = "disabled-check";
    };
  };
  for (var i = 0; i < notSelectedNodes.length; i++) {
    var currentNode = notSelectedNodes[i];
    var currentNodeRealName = currentNode.data.realName;
    if (currentNodeRealName == nodeExclude && !currentNode.isSelected()) {
      currentNode.data.unselectable = true;
      currentNode.li.className = "disabled-check";
    };
  };
}

function removeUnselectableRequireParent(nodeRequire, allNodes){
  for (var i = 0; i < allNodes.length; i++) {
    var currentNode = allNodes[i];
    var currentNodeRealName = currentNode.data.realName;
    if (currentNodeRealName == nodeRequire) {
      currentNode.data.unselectable = false;
      currentNode.li.className = "";
    };
  };
}

function removeUnselectableRequire(nodeRequire, selectedNodes){
  for (var i = 0; i < selectedNodes.length; i++) {
    var currentNode = selectedNodes[i];
    var currentNodeRealName = currentNode.data.realName;
    if (currentNodeRealName == nodeRequire && currentNode.isSelected()) {
      currentNode.data.unselectable = false;
      currentNode.li.className = "";
    };
  };
}

function removeUnselectableExclude(nodeExclude, notSelectedNodes){
  for (var i = 0; i < notSelectedNodes.length; i++) {
    var currentNode = notSelectedNodes[i];
    var currentNodeRealName = currentNode.data.realName;
    if (currentNodeRealName == nodeExclude && !currentNode.isSelected()) {
      currentNode.data.unselectable = false;
      currentNode.li.className = "";
    };
  };
}

function checkIfSameThenSelect(realName, notSelectedNodes){
  for (var i = 0; i < notSelectedNodes.length; i++) {
    if (notSelectedNodes[i].data.realName == realName) {
      notSelectedNodes[i].toggleSelect();
    };
  };
}

function renameFeatureTitle(allNodes){
  for (var i = 0; i < allNodes.length; i++) {
    var currentNode = allNodes[i];

    if (currentNode.data.require != null || currentNode.data.exclude != null){
      var nodeRealName = currentNode.data.realName;
      var nodeTitle = currentNode.data.title;
      for (var y = 0; y < allNodes.length; y++) {
        if (nodeRealName == allNodes[y].data.realName) {
          if (nodeTitle != allNodes[y].data.title) {
            var currentClassName = allNodes[y].li.className;
            allNodes[y].setTitle(nodeTitle);
            if (currentNode.data.require != null) {
              allNodes[y].setRequire(currentNode.data.require);
              allNodes[y].li.className = currentClassName;
            };
            if (currentNode.data.exclude != null) {
              allNodes[y].setExclude(currentNode.data.exclude);
              allNodes[y].li.className = currentClassName;
            };
          };
        };
      };
    }
  };
}

function selectWithSameName(node, notSelectedNodes){
  for (var i = 0; i < notSelectedNodes.length; i++) {
    if (notSelectedNodes[i].data.realName == node.data.realName && node.data.children == undefined && !notSelectedNodes[i].isSelected()) {
      notSelectedNodes[i].toggleSelect();
    };
  };
}

function deSelectWithSameName(node, selectedNodes){
  for (var i = 0; i < selectedNodes.length; i++) {
    if (selectedNodes[i].data.realName == node.data.realName && node.data.children == undefined && selectedNodes[i].isSelected()) {
      selectedNodes[i].toggleSelect();
    };
  };
}

function toggleChild(node, otherGroup, select){
  for (var i = 0; i < otherGroup.length; i++) {
    if (otherGroup[i].data.realName == node.data.realName) {
      if (select && !otherGroup[i].isSelected()) {
        otherGroup[i].toggleSelect();
      }
      else if ((!select && otherGroup[i].isSelected())) {
        otherGroup[i].toggleSelect();
      }
    };
  };
}

function toggleDisableButton(){
  var downloadButton = document.getElementById("download");
  // if(selFeatures.length == 0){
  // 	downloadButton.attr('disabled', 'disabled');
  // }
  // else{
  // 	downloadButton.attr('disabled', 'disabled');
  // }
  //console.log(downloadButton.attr('class'))
}

function firstToUpperCase(str) {
  return str.substr(0, 1).toUpperCase() + str.substr(1);
}

/*
 * catatan:
 * -	[done] 	saat ini: var currentNodeTitle = currentNode.data.title;
 * 		   	untuk kasus yang title dan nama asli fitur nya beda (cth: case di group by constraints)
 *		   	saran solusi: ditambahin di JSONnya, atribut realName utk masing2 datanya
 * - [done] 	ada kasus dimana require dan exclude lebih dari satu fitur
 * 		   	saran solusi: pisahin pake koma (,) dan nanti displit utk masing2 nama fiturnya
 * - [done] 	fitur yang ada di suatu grup akan ikut ter-select jika ada fitur di grup lain
 *		   	yang sudah ter-select dari awal
 * - [done] 	untuk fitur di grup yg dari awal memang udah ter-select, harus dicek lagi
 *		   	sesuai dengan aturan exclude dan require
 * - [done] 	fitur (sifitur) yang punya require (sirequire), sirequire-nya itu otomatis ter-select dan tdk bisa
 *		   	diubah2 lagi, kecuali sifitur di-unselect
 * - [done]	samain nama dan require/exclude setiap fitur (yang punya exclude atau require) di semua grup
 * - [done] 	tiap ada fitur di suatu grup diklik, fitur di grup lain dengan nama yang sama harus juga ikut ke klik
 * - [done]	masih error pas setelah di create trus kalo klik grup. hipotesa nya adalah sewaktu grup/folder nya di klik,
 *		   	dynatree akan ngejalananin fungsi utk men-uncheck seluruh children nya terlebih dahulu
 *			baru abis itu masuk ke option/fungsi onSelect yang di atas. hal ini membuat pas mau manggil fungsi 
 *			removeUnselectableRequire, yg dikirim bukan node2 yang seharusnya (yang di grup tsb dan terdaftar sebagai 
 *			checked), tapi yang dikirim adalah node2 lainnya yg bukan children dari grup itu, karena children-nya 
 *			udah ter-uncheck semua.
 * - []		kalo di-check/uncheck dari grup/folder, setiap fitur yang bersesuaian (di grup lain) dengan children (di grup
 *			tsb) harus ikut ter-check/uncheck
 */
