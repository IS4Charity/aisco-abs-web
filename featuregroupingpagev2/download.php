<?php
require __DIR__ . '/vendor/autoload.php';

// Loading Libraries
$dotenv = Dotenv\Dotenv::create(__DIR__.DIRECTORY_SEPARATOR."..");
$dotenv->load();

// =============================================== self libraries
function randomPassword() {
  $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
  $pass = array(); //remember to declare $pass as an array
  $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
  for ($i = 0; $i < 8; $i++) {
    $n = rand(0, $alphaLength);
    $pass[] = $alphabet[$n];
  }
  return implode($pass); //turn the array into a string
}

function getFreePort(){

  error_reporting(~E_ALL);

  $from = 8000;
  $to = 10000;

  //TCP ports
  $host = 'localhost';

  for($port = $from; $port <= $to ; $port++)
  {
    $fp = fsockopen($host , $port);
    if (!$fp)
    {
      fclose($fp);
      return $port;
    }
  }

  throw new Exception('no port available');
}

// =============================================== build sequence


$servername = getenv('DBHOST');
$dbname = getenv('DBNAME');
$username = getenv('DBUSERNAME');
$password = getenv('DBPASSWORD');
$project_root =  getenv('PROJECT_ROOT');
$aisco_product_table = getenv('AISCO_PRODUCT_TABLE');
$subdomain =  getenv('SUB_DOMAIN');
if($subdomain != '') $subdomain .= ".";



$dbproductname = $_POST['dbname'];
/* $productname = $_POST['productname']; */
$orgname = $_POST['orgname'];
$adminname = $_POST['adminname'];
$adminemail = $_POST['adminemail'];
$product_name = $_POST['productname'];

$response = [];
$response['messages'] = '';
$general_log_path= $project_root.'/generated_products/'.$product_name.'/all-status.txt';;

$temp = "\n=============== INPUT .... \n";
$response['messages'] .= $temp;
file_put_contents($general_log_path, $temp);

$temp = '1. :'. $_POST['dbname']."\n";
file_put_contents($general_log_path, $temp, FILE_APPEND);
$response['messages'] = $temp;
$temp = '2. :'. $_POST['orgname']."\n";
file_put_contents($general_log_path, $temp, FILE_APPEND);
$response['messages'] = $temp;
$temp = '3. :'. $_POST['adminname']."\n";
file_put_contents($general_log_path, $temp, FILE_APPEND);
$response['messages'] = $temp;
$temp = '4. :'. $_POST['adminemail']."\n";
file_put_contents($general_log_path, $temp, FILE_APPEND);
$response['messages'] = $temp;
$temp = '5. :'. $_POST['productname']."\n";
file_put_contents($general_log_path, $temp, FILE_APPEND);
$response['messages'] = $temp;
$temp = '-. raw:'. json_encode($_POST)."\n";
file_put_contents($general_log_path, $temp, FILE_APPEND);
$response['messages'] = $temp;

//$dbproductname = 'matamata';
//$orgname = 'matamata';
//$adminname = 'matamata';
//$adminemail = 'mata@mata.com';

/* $adminpassword = randomPassword(); */
$adminpassword = 'admint123';
$product_dir = $dbproductname;


$temp = "\n=============== Insert Product Data To DB .... \n";
$response['messages'] .= $temp;
file_put_contents($general_log_path, $temp, FILE_APPEND);

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);

// Check connection
if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}
$temp = "Connected successfully\n";
$response['messages'] .= $temp;
file_put_contents($general_log_path, $temp, FILE_APPEND);

if(empty($_POST['filename']) || empty($_POST['content'])){
  $temp = "no filename\n";
  $response['messages'] .= $temp;
  file_put_contents($general_log_path, $temp, FILE_APPEND);
  $cmd .= "echo 'download.php fail ' > ".$project_root."/downloadphpfail";
  $output = shell_exec($cmd." 2>&1");
  $temp = $output;
  $response['messages'] .= $temp;
  file_put_contents($general_log_path, $temp, FILE_APPEND);
  #exit;
}



// CREATE ABS FILE
$filename = preg_replace('/[^a-z0-9\-\_\.]/i','',$_POST['filename']);
$path_to_file = $project_root.'/src/abs/product/';
$newFileName = $path_to_file.$_POST['filename'];
if (file_put_contents($newFileName, $_POST['content'], FILE_APPEND) !== false) {
  $temp = "File created (" . basename($newFileName) . ")\n";
  $response['messages'] .= $temp;
  file_put_contents($general_log_path, $temp, FILE_APPEND);
} else {
  $temp = "Cannot create file (" . basename($newFileName) . ")\n";
  $response['messages'] .= $temp;
  file_put_contents($general_log_path, $temp, FILE_APPEND);
}


$filename_without_ext = str_replace(".abs","",$_POST['filename']);


$temp = "\n=============== Talking to DB .... \n";
$response['messages'] .= $temp;
file_put_contents($general_log_path, $temp, FILE_APPEND);

// WARNING PRONE TO SQL INJECTION
// TODO: Use proven library
$port = getFreePort();
$insert_product_sql = "INSERT INTO  ". $aisco_product_table ." (org_name, admin_name, admin_email, admin_password, name, db_name, port) VALUES ('".$orgname."','".$adminname."','".$adminemail."','".$adminpassword."','".$filename_without_ext."','".$dbproductname."','".$port."');";


// REGISTER CREATED PRODUCT
if ($conn->query($insert_product_sql) === TRUE) {
  $temp = "New record created successfully\n";
  $response['messages'] .= $temp;
  file_put_contents($general_log_path, $temp, FILE_APPEND);
} else {
  $temp = "Error: " . $insert_product_sql . "<br>" . $conn->error . "\n";
  $response['messages'] .= $temp;
  file_put_contents($general_log_path, $temp, FILE_APPEND);
}


$conn->close();


$temp = "\n=============== Make project dir .... \n";
$response['messages'] .= $temp;
file_put_contents($general_log_path, $temp, FILE_APPEND);

$cmd = "rm -r ".$project_root."/generated_products/".$product_name." 2>&1 ;";
$cmd .= "mkdir ".$project_root."/generated_products/".$product_name." 2>&1 ;";
$cmd .= "mkdir ".$project_root."/target 2>&1 ;";
$output = shell_exec($cmd." 2>&1");
$temp .= $output."\n";
$response['messages'] .= $temp;
file_put_contents($general_log_path, $temp, FILE_APPEND);



$temp = "\n=============== Configuring auth & config .... \n";
$response['messages'] .= $temp;
file_put_contents($general_log_path, $temp, FILE_APPEND);

chdir($project_root);

// CONFIG PROP
$lines = file('config.properties');
$word = $dbproductname;
$result = '';

# reconstruct the file, add dbname information
foreach($lines as $line) {
  if(substr($line, 0, 7) == 'dbname=') {
    $result .= 'dbname='.$word."\n";
  } else {
    $result .= $line;
  }
}

$response['messages'] .= $result;
$temp = 'config.properties path: '.$project_root.'/config.properties';
file_put_contents($general_log_path, "$temp\nconfig properties content: \n$result\n", FILE_APPEND);

// we use below, for building purposes
if(file_put_contents($project_root.'/config.properties',$result)){
  file_put_contents($general_log_path, "putting to config.properties success\n", FILE_APPEND);
}else{
  file_put_contents($general_log_path, "putting to config.properties failed (make sure permission is www-data)\n", FILE_APPEND);
}

// to run a product we need to put this in the following file
file_put_contents($project_root.'/generated_products/'.$product_name.'/config.properties',$result);



// AUTH PROP
$adminauth = $adminemail . "=" . md5($adminpassword) . ":admin";
// load the data and delete the line from the array
$lines = file('auth.properties');
$last = sizeof($lines) - 1 ;
unset($lines[$last]);

$fp = fopen('auth.properties', 'w');
fwrite($fp, implode('', $lines));
fclose($fp);
file_put_contents($project_root.'/generated_products/'.$product_name.'/auth.properties',$adminauth.PHP_EOL , FILE_APPEND | LOCK_EX);

// AISCO CONF
// remove prev equal settings

$cmd = "mkdir ".$project_root."/virtual_hosts/;";
$cmd .= "rm  ".$project_root."/virtual_hosts/".$word.".conf 2>&1 ;";
$output = shell_exec($cmd." 2>&1");

$domain = "$word.$subdomain"."aiscoweb.org";
$config = "NameVirtualHost *:80\n".
  "<VirtualHost *:80>\n".
  "\tProxyPreserveHost On\n".
  "\tServerName $domain\n".
  "\tProxyPass / http://localhost:".$port."/\n".
  "\tProxyPassReverse / http://localhost:".$port."/\n".
  "</VirtualHost>\n";
file_put_contents($project_root.'/virtual_hosts/'.$word.'.conf',$config.PHP_EOL ); 




$temp = "\n=============== Building .... \n";
$response['messages'] .= $temp;
file_put_contents($general_log_path, $temp, FILE_APPEND);

// build the product
// $command = "ant -Dabsproduct=".$filename_without_ext." abs.deploy";
$cmd = "bash build.sh ".$product_name;
$output = shell_exec($cmd." 2>&1");
$temp = $output;
$response['messages'] .= $temp;
file_put_contents($general_log_path, $temp, FILE_APPEND);
file_put_contents($project_root.'/generated_products/'.$product_name.'/build-status.txt', $output);

if (strpos($output, 'build failed !!') !== false) {
  $response['status'] = 'failed';
  if (strpos($output, 'Constraints failed') !== false) {
    $response['status_code'] = 1;
  }else{
    $response['status_code'] = 0;
  }
  echo json_encode($response);
  return;
}
if (strpos($output, 'build done!') === false) return;

$temp = "\n\nstopping existing runnning instance..";
$response['messages'] .= $temp;
file_put_contents($general_log_path, $temp, FILE_APPEND);
$cmd = "bash $project_root/scripts/stop-instance.sh '$product_name'";
$output = shell_exec($cmd." 2>&1");
$temp = $output."\n";
$response['messages'] .= $temp;
file_put_contents($general_log_path, $temp, FILE_APPEND);


$temp = "\n=============== Deploy .... \n";
$response['messages'] .= $temp;
file_put_contents($general_log_path, $temp, FILE_APPEND);
/* $cmd = "sudo /etc../init.d/apache2 reload"; */

$temp = "running app..\n";
$response['messages'] .= $temp;
file_put_contents($general_log_path, $temp, FILE_APPEND);
$cmd = "bash run.sh ".$product_name." ".$port." ";
$log_path = $project_root.'/generated_products/'.$product_name.'/deploy-status.txt';
$output = shell_exec($cmd." 2> ".$log_path." > ".$log_path." &");
$temp = $output;
$response['messages'] .= $temp;
file_put_contents($general_log_path, $temp, FILE_APPEND);
file_put_contents($log_path, $temp, FILE_APPEND);

$temp = "probing app..\n";
$response['messages'] .= $temp;
file_put_contents($general_log_path, $temp, FILE_APPEND);
$cmd = "wget localhost:$port -O /dev/null";
$log_path = $project_root.'/generated_products/'.$product_name.'/probing-status.txt';
$output = shell_exec($cmd." 2> ".$log_path." > ".$log_path."");
$temp = $output;
$response['messages'] .= $temp;
file_put_contents($general_log_path, $temp, FILE_APPEND);
file_put_contents($log_path, $temp, FILE_APPEND);

$temp = "reloading apache... \n";
$response['messages'] .= $temp;
file_put_contents($general_log_path, $temp, FILE_APPEND);
$cmd = "sudo $project_root/scripts/reload-apache.sh";
//$cmd = "/usr/sbin/service apache2 reload";
$output = shell_exec($cmd." 2>&1");
$temp = $output;
$response['messages'] .= $temp;
file_put_contents($general_log_path, $temp, FILE_APPEND);


// ================================================= MAILING

$isMail = (getenv('IS_MAIL') == 'TRUE');

if($isMail){
  $temp = "\n======================== Mailing \n";
  $response['messages'] .= $temp;
  file_put_contents($general_log_path, $temp, FILE_APPEND);

  $mail_headers = "From: admin@aiscoweb.org \r\n";
  $mail_subject = "[AISCO] $product_name is successfully prepared";
  $mail_messages = "
Hi $adminname, your product is ready, you can visit on http://".$word.$subdomain.".aiscoweb.org.

username: ".$adminemail."
password: admint123

if you have any question please contact us via email ade@cs.ui.ac.id.
Thank you very much for using our services.

Best Regards,
\n\nAISCOWEB Team";

  if(mail($adminemail,$mail_subject, $mail_messages, $mail_headers)){
    $temp = "\nEmail successfully sent\n";
    $response['messages'] .= $temp;
    file_put_contents($general_log_path, $temp, FILE_APPEND);
  } else{
    $temp = "\nError sending email\n";
    $response['messages'] .= $temp;
    file_put_contents($general_log_path, $temp, FILE_APPEND);
  }
}

$response['status'] = 'success';
$response['status_code'] = 200;
echo json_encode($response);


//header('Location: /success.php'); 
?>
