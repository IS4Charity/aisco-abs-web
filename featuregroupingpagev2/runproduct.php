<?php
require __DIR__ . '/vendor/autoload.php';
// Loading Libraries
$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

$servername = getenv('SERVERNAME');
$dbname = getenv('DBNAME');
$username = getenv('DBUSERNAME');
$password = getenv('DBPASSWORD');
$project_root =  getenv('PROJECT_ROOT');
$aisco_product_table = getenv('AISCO_PRODUCT_TABLE');

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT admin_email, admin_name, admin_password, name, db_name, port FROM product where is_running=0";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    try {
      $productName = ucwords($row["db_name"]);
      $productDir = $project_root."/generated_products/".$productName."/";
      chdir($productDir);

      $sqlupdate = "update product set is_running=1 where port='".$row["port"]."'";
      $conn->query($sqlupdate);

      $command = "nohup java -jar ".$row["name"].".jar -p ". $row["port"].
        " > ".$productDir."/running-status ".
        " 2>".$productDir."/running-status &";

      shell_exec($command);

      sleep(10*60); //delay 10 minutes

      /* if(mail($row["admin_email"],$row["name"]." Has Succesfully Created","Hi ". $row["admin_name"]. ",\n\nYour product is successfully created \nFor access your product please access this URL  ". $row["db_name"] .".aiscoweb.org by using following account\n\n\nusername : ".$row["admin_email"]."\npassword : ".$row["admin_password"]."\n\n\nPlease mind your password, if you lost, please inform us by sending email to aisco.rse@gmail.com and we will send you new password.\n\n\nThanks,\n\nAISCOWEB Team\n\n\n\n\n\nnote:\nthis is generated email from machine")) */
      /* print "Email successfully sent"; */
      /* else */
      /* print "An error occured"; */
    } catch (Exception $e) {
      file_put_contents('/home/aisco/aisco-abs-web/status.txt', $e->getMessage().PHP_EOL , FILE_APPEND | LOCK_EX);
    }
  }
} else {
  echo "0 results\n";
}

$conn->close();
?>
