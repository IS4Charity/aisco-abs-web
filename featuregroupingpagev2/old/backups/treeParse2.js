var treeData = null;
//var productSel = "";
var selFeatures = [];
$.ajax('dataCharity3.json').done(function(result){
	treeData = result;
	$(function(){
		$("#treeCoba").dynatree({
			checkbox: true,
			selectMode: 3,
			children: treeData,
			onSelect: function(select, node) {
				var selKeys = $.map(node.tree.getSelectedNodes(), function(node){return node.data.key;});
				var allselected = selKeys.join(",").toString();
				var firstarr = allselected.split(",");
				var secondarr = allselected.split(",");
				var outnode = temp = "";
				var i, y;
				var viewsArr = [];

				for(y = 0; y<firstarr.length; y++) {
					outnode += '<span>' + firstarr[y] + '</span><br>';
				}
				//document.getElementById("selectednodes").innerHTML = outnode;

				for(i = 0; i<secondarr.length; i++) {
					if (secondarr[i].indexOf("[child]") >= 0) {
						var feat = secondarr[i].substring(8);
						var view = '<span>' + feat + '</span><br>';

						viewsArr.push(view);


						//out += '<span>' + feat + '</span><br>';
						//temp +=  feat;
						selFeatures.push(feat);
					}
				}
				//productSel = temp;
				var uniqueViewArr = getUnique(viewsArr);
				var out = "";
				for (var i = 0; i < uniqueViewArr.length; i++) {
					out += uniqueViewArr[i];
				};
				document.getElementById("selectedchilds").innerHTML = out;
			}
		});
	});
});

function getProduct(){
	var features = getUnique(selFeatures);
	var res = "";
	for (var i = 0; i < features.length; i++) {
		if (i != features.length-1) {
			res += features[i] + ", ";
		}
		else{
			res += features[i];
		}
	};
	return "// This product was generated automatically \n product ResultProduct (" + res + ");";
}

function getUnique(array) {
    var unique = [];
    for (var i = 0; i < array.length; i++) {
        if (unique.indexOf(array[i]) == -1) {
            unique.push(array[i]);
        }
    }
    return unique;
};