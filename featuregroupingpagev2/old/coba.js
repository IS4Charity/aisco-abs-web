myFunction([
	{title: "Satu", isFolder: true, key: "Satu",
		children: [
			{title: "Dua", key: "Dua",
				children: [
					{title: "Tiga", key: "Tiga" },
					{title: "Empat", key: "Empat" }
				]
			},
			{title: "Lima", key: "Lima",
				children: [
					{title: "Enam", key: "Enam" },
					{title: "Tujuh", key: "Tujuh" }
				]
			}
		]
	},
	{title: "Delapan", key: "Delapan", isFolder: true,
		children: [
			{title: "Sembilan", key: "Sembilan",
				children: [
					{title: "Sepuluh", key: "Sepuluh" },
					{title: "Sebelas", key: "Sebelas" }
				]
			},
			{title: "Dua Belas", key: "Dua Belas",
				children: [
					{title: "Tiga Belas", key: "Tiga Belas" },
					{title: "Empat Belas", key: "Empat Belas" }
				]
			}
		]
	}
])