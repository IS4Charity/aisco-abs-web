
$(document).ready(function(){$(".alert").addClass("in").fadeOut(4500);

  /* swap open/close side menu icons */
  $('[data-toggle=collapse]').click(function(){
    // toggle icon
    $(this).find("i").toggleClass("glyphicon-chevron-right glyphicon-chevron-down");
  });
});

// advanced textarea
tinyMCE.init({ 
  selector: "textarea.tinymce",
  height: '400'
});

// Share button
stLight.options({publisher: "d114feb2-bdb0-4cbb-ab85-7f0e895b4a58", doNotHash: false, doNotCopy: false, hashAddressBar: false});

