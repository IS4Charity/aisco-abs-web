# build this docker file with
# docker build -t registry.gitlab.com/is4charity/aisco-abs-web .

FROM ubuntu:latest

# Install system dependencies
RUN apt-get update 
RUN apt-get -y install curl php-json php-cgi
RUN apt-get -y install unzip
RUN apt-get -y install zip


# install other optional
RUN apt-get -y install git
RUN apt-get -y install vim

# install apache
RUN apt-get -y install apache2

# install php
RUN apt-get -y install php7.2 libapache2-mod-php7.2 php-mysql
# php extension, laravel requirement
#RUN apt-get -y install php7.2-mbstring php7.2-gd php7.2-xml php7.2-curl

# install mysql
RUN apt-get -y install mysql-client

# install java 
RUN apt-get -y install software-properties-common # add-apt-repository dependency

# since April 2019, this ppa is discontinued
# RUN add-apt-repository -y ppa:webupd8team/java # since April 2019, this ppa is discontinued
# RUN apt-get update
# auto answer yes
# RUN echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections
# RUN echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections
# RUN apt-get -y install oracle-java8-installer

# install java
RUN apt-get -y install default-jre

# install ant
RUN apt-get -y install ant

# install composer 
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# install sendmail 
RUN apt-get -y install sendmail
RUN apt-get -y install ssmtp

# to enable apache restart
RUN apt-get -y install sudo

# enable modules
RUN a2enmod rewrite
RUN a2enmod proxy
RUN a2enmod proxy_http

# enable wget
RUN apt-get -y install wget

#CMD apache2 -D FOREGROUND


#WORKDIR /var/www
