this_filedir_path="$(readlink -f $0 | xargs dirname)"
echo "%www-data ALL=(ALL) NOPASSWD: $this_filedir_path/../scripts/reload-apache.sh" >> /etc/sudoers
echo "%www-data ALL=(ALL) NOPASSWD: /usr/sbin/service apache2 reload" >> /etc/sudoers

# echo "%www-data ALL=(ALL) NOPASSWD: $this_filedir_path/stop-instance.sh" > /etc/sudoers.d/aisco_stop_instance
# echo "www-data ALL=(ALL) NOPASSWD: /bin/kill" >> /etc/sudoers.d/aisco_stop_instance

#echo "%www-data ALL=(ALL) NOPASSWD: /usr/bin/tee" > /etc/sudoers.d/aisco_update_etc_hosts
#echo "%www-data ALL=(ALL) NOPASSWD: /bin/sed" >> /etc/sudoers.d/aisco_update_etc_hosts
