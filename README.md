# Table of Content

1. [Getting Started Docker](#getting-started-docker)
    - [Cloning](#cloning-docker)
    - [Setup Environtment](#setup-environtment-docker)
    - [Setup Permission](#setup-permission-docker)
    - [Install System Dependency](#install-system-dependency-docker)
    - [Used Port](#used-port-docker)
    - [Build a Product](#build-a-product-docker)
    - [Run a Product](#run-a-product-docker)
    - [Modifying Database](#modifying-database-docker)
2. [Getting Started Non Docker](#getting-started-non-docker)
    - [Cloning](#cloning-non-docker)
    - [Setup Environtment](#setup-environtment-non-docker)
    - [Setup Permission](#setup-permission-non-docker)
    - [Install System Dependency](#install-system-dependency-non-docker)
    - [Build a Product](#build-a-product-non-docker)
    - [Run a Product](#run-a-product-non-docker)
3. [Basic Knowledge](#basics)
    - [Directory Structure Overview](#directory-structure-overview)
    - [Routing](#routing)
    - [Controllers](#controllers)
    - [Models](#models)
    - [Views](#views)
    - [Foreign Language Interface (FLI)](#foreign-language-interface)
    - [Naming Convention](#naming-convention)
    - [Feature Model](#feature-model)
    - [Feature Model Tutorial](#feature-model-basic-tutorial)
    - [Feature Grouping](#feature-grouping-tutorial)
    - [Creating New Menu by Delta](#creating-new-menu-by-delta)
    - [Creating New Menu by Delta](#creating-new-menu-by-delta)
4. [Architectures](#architectures)
    - [Feature Grouping Page](#feature-grouping-page)
    - [Build Process](#build-process)
    - [Message flow](#message-flow)
    - [Terminology tree](#terminology-tree)
    - [RSE Server Arch](#rse-server-arch-relevant-only-for-rse-lab-member-)
5. [Advanced Knowledge](#advanced-knowledge)
6. [Common Issue](#common-issue)
7. [About the Framework](#about)
8. [Demo](#demo)
9. [Links](#links)

<br/>

# Getting Started - Docker

### Cloning - Docker
- Clone the [repository](https://gitlab.com/IS4Charity/aisco-abs-web)
- Go to the branch `master` , by executing:

    `git pull origin `

### Setup Environtment - Docker
- `cp .env.example .env`
- adjust `featuregroupingpagev2/.env.example` (change `SUB_DOMAIN` to staging if required)
- `cp featuregroupingpagev2/.env.example featuregroupingpagev2/.env`
- adjust `config.properties.example`
- `cp .config.properties.example config.properties`
- `cp .auth.properties.example auth.properties`

### Setup Permission  - Docker
- These folder must be writable by `www-data` group 
  - ./ # project root
  - config.properties
  - auth.properties
  - generated_products/
  - src/abs/product/
  - src/abs/model/
  - src/java/
  - target/
  - lib/lib/absdborm.jar
- You can use [permission-script.sh](scripts/permission-script.sh) for quicker adjustment.  Execute this script from your `project_root`

### Install System Dependency - Docker

1. Install docker
    - `apt-get install docker`
    - `apt-get install docker-compose`
    - In case it fail, do the followings:
      - create docker group: `sudo groupadd docker`
      - add your user to the docker group `sudo usermod -aG docker $USER`
      - logout and login again, if not work try to reboot
      - if you don't like reboot, try the this [workaround](https://superuser.com/questions/272061/reload-a-linux-users-group-assignments-without-logging-out) question
    - Make sure you are not running the docker behind a proxy, if so, you need to do this [steps](https://stackoverflow.com/questions/23111631/cannot-download-docker-images-behind-a-proxy).
2. Start docker orchestrator
    - `docker-compose -f docker-compose-local.yml up `
        - This command will try to download all the required images, and the size is quite big, so we recommend to do it in a free wifi zone.
          - Main image ± 500MB
          - Mariadb ± 300MB
    - In case it fail, wait until `aisco-db` container fully started then retry again
    - In case it fail with error messages ` standard_init_linux.go:207`, it means you need to rebuild the docker image 
        - execute the followings:`docker build -t registry.gitlab.com/is4charity/aisco-abs-web .`

### Used Port - Docker
- http://localhost:8000/featuregroupingpagev2 => to access product selection page (in local environtment cannot build & run product automaticaly, you need to do it [manually](#build-a-product))
- http://localhost:7999/ => to access phpmyadmin
- http://localhost:8001/ => to access generated product (need to be [build](#build-a-product) & [run](#run-a-product))

** Used port based on .env file

### Build a Product - Docker

You can do it automatically by visiting the featuregroupingpagev2, or manually by using the following steps:

1. Enter the container `docker exec -it aisco-app /bin/bash`

2. Go to product directory in `/var/www/html/src/abs/product`

3. Copy `/var/www/html/src/abs/product/.Product.abs.example` into `/var/www/html/src/abs/product/Product.abs`
    - here you can specify the feature for the product, all available feature could be seen in `/var/www/html/src/abs/framework/ProductLineConfig.abs`

4. Back to project root `/var/www/html` (still in docker container)

5. Make sure these files & folders present

```
project_root/
+-- build.sh                   (initiate the build process)
+-- build.xml                  (contain instruction on how to bundle the product)
+-- config.properties          (specify the database credential) 
+-- src/                       (contain abs source code to be compiled)
+-- target/                    (temporary folder)
+-- lib/                       (library used to compile the source code)
```

6. Then execute the following:

`bash build.sh Product` 

The output will be putted in `generated_products` directory

### Run a Product - Docker

1. Enter the container `docker exec -it aisco-app /bin/bash`

2. Go to the product directory in `/var/www/html/generated_products/[ProductName]/`.

3. Make sure that these files & folders are present:

```
/var/www/html/generated_products/[ProductName]/
...
+-- config.properties          (this file specify the database credential) 
+-- auth.properties            (contain credential to log in to the system)
+-- external/                  (contain hardcoded html,css,js to be used)
+-- lib/                       (library used to run the jar)
+-- [Product Name].jar         (product jar, builded from abs source code)
```

##### If you run it on localhost do the followings:

4. Execute:

`java -jar [Product Name].jar -p 8001'`

** in case of using the docker, the port should be 8001, since it is the only exposed port to the host.

5. visit `http://localhost:8001` to view the product. login by using
  - id: `admin@localhost.com`
  - pass : `admint123`

##### If you run it on server, you need to configure the `virtual_hosts` configurations

6. go to `project_root/virtual_hosts`, copy 
```
NameVirtualHost *:80
<VirtualHost *:80>
	ProxyPreserveHost On
	ServerName [product name].aiscoweb.org
	ProxyPass / http://localhost:[any desired port]/
	ProxyPassReverse / http://localhost:[any desired port]/
</VirtualHost>
```

7. Execute:

`java -jar [Product Name].jar -p [your desired port]'`

8. Reload apache configurations

`service apache2 reload`


### Modifying Database - Docker

1. Enter the container `docker exec -it aisco-app /bin/bash` or `bash scripts/enter-docker.sh`

2. run `mysql -u root -h aisco-db` or `bash scripts/enter-mysql.sh`

# Getting Started - Non-Docker

### Cloning - Non-Docker
- Clone the [repository](https://gitlab.com/IS4Charity/aisco-abs-web)
- Go to the branch `master` , by executing:

    `git pull origin `

### Setup Environtment - Non-Docker
- `cp .env.example .env`
- `cp featuregroupingpagev2/.env.example featuregroupingpagev2/.env`
- `cp .config.properties.example config.properties`
- `cp .auth.properties.example auth.properties`

### Setup Permission  - Non-Docker
- These folder must be writable by `www-data` group 
  - ./ # project root
  - config.properties
  - auth.properties
  - generated_products/
  - src/abs/product/
  - src/abs/model/
  - src/java/
  - target/
  - lib/lib/absdborm.jar
- You can use [permission-script.sh](scripts/permission-script.sh) for quicker adjustment. Execute this script from your `project_root`

### Install System Dependency - Non-Docker
- LAMP(linux-apache-msyql-php) stack 
    - Apache: `apt-get -y install apache2`
    - Php: `apt-get -y install php7.2 libapache2-mod-php7.2 php-mysql`
    - Mysql: `apt-get -y install mysql-client`
- Java development kit
    - Install custom repository: `apt-get -y install software-properties-common`
    - Add custom repository: `add-apt-repository -y ppa:webupd8team/java`
    - Instal Java: `apt-get -y install oracle-java8-installer`
    - [Full Tutorial](https://www.linode.com/docs/development/java/install-java-on-ubuntu-16-04/)
- Ant build tools
  - `sudo apt-get install ant`
- Composer
  - `sudo apt-get install composer`
- Install 3rd party library 
  - `cd featuregroupingpagev2`
  - `composer install`
- Create database aisco_administration
- Migrate database aisco_administration.sql
  - `mysql -u [username] -p aisco_administration < aisco_administration.sql` (working direktori ada di : ./featuregroupingpagev2)


### Build a Product - Non-Docker

1. Go to product directory in `/var/www/html/src/abs/product`

2. Copy `/var/www/html/src/abs/product/.Product.abs.example` into `/var/www/html/src/abs/product/Product.abs`
    - here you can specify the feature for the product, all available feature could be seen in `/var/www/html/src/abs/framework/ProductLineConfig.abs`

3. Back to project root `/var/www/html` (still in docker container)

4. Make sure these files & folders present

```
project_root/
+-- build.sh                   (initiate the build process)
+-- build.xml                  (contain instruction on how to bundle the product)
+-- config.properties          (specify the database credential) 
+-- src/                       (contain abs source code to be compiled)
+-- target/                    (temporary folder)
+-- lib/                       (library used to compile the source code)
```

5. Then execute the following:

`bash build.sh Product` 

The output will be putted in `generated_products` directory


### Run a Product - Non-Docker

1. Go to the product directory in `project_root/generated_products/[ProductName]/`.

2. Move `project_root/auth.properties` to `project_root/generated_products/[ProductName]/`

3. Move `project_root/config.properties` to `project_root/generated_products/[ProductName]/`

4. Make sure that these files & folders are present: Remember to move auto.properties and config.properties from home to the product folder.

```
/var/www/html/generated_products/[ProductName]/
...
+-- config.properties          (this file specify the database credential) 
+-- auth.properties            (contain credential to log in to the system)
+-- external/                  (contain hardcoded html,css,js to be used)
+-- lib/                       (library used to run the jar)
+-- [Product Name].jar         (product jar, builded from abs source code)
```

##### If you run it on localhost do the followings(2):

5. Execute:

`java -jar [Product Name].jar -p [any unused port]'`

6. visit `http://localhost:[choosen port]` to view the product. login by using
  - id: `admin@localhost.com`
  - pass : `admint123`


##### If you run it on server, you need to configure the `virtual_hosts` configurations

7. go to `project_root/virtual_hosts`, copy 
```
NameVirtualHost *:80
<VirtualHost *:80>
	ProxyPreserveHost On
	ServerName [product name].aiscoweb.org
	ProxyPass / http://localhost:[any desired port]/
	ProxyPassReverse / http://localhost:[any desired port]/
</VirtualHost>
```

8. Execute:

`java -jar [Product Name].jar -p [your desired port]'`

9. Reload apache configurations

`service apache2 reload`


# Basics

## File Structure Overview

* auth.properties: store credentials to login into generated products
* config.properties: store database credentials for generated products
* .env: store database credentials for featuregroupingpagev2
* src/abs/controller/: Store the controllers logic in ABS language
* src/abs/view/: Store the HTML files
* src/abs/model/: Store the models logic in ABS language
* src/abs/framework/: 
    * src/abs/framework/ProductLineConfig.abs: Map the features and the delta
    * src/abs/framework/FeatureModel.abs: Define the feature constraints
    * src/abs/framework/Route.abs: Define http routing
* src/abs/delta/: Store the deltas

* docker/: Store the supporting file for docker
* external/: Store the supporting libraries for product's user interface (UI)
* generated_products/: Store the generated product
* featuregroupingpagev2/: Store the product selection page & processor
* lib/: Store the libraries
* target/: Store the temporary file

## Routing
Routes are defined on `src/abs/framework/Route.abs`

To add a new route, please define the following entry:

  `[url-segment]` => `[scope]:[Module].[ImplementationClass]@[MethodName]`

  available `[scope]`: all, admin

example:

  `/admin/expense/update.abs` => `all:MExpenseController.ExpenseControllerImpl@update`;

## Controllers
Controllers are available on `src/abs/controllers`

To add new controller, please do the followings:

1. Define a module name
2. Import the required library
3. Define an interface
4. Define an implementation class. 
    1. For passing datas to view, return a Pair where the first argument is the view path, and the second is the data.
      - example: ``return Pair("automatic-report/edit", dataModel);``
    2. For passing datas to view with view model, return a Triple where the first argument is similar with the previous while the last argument is the view model
      - example: ``return Triple("program/list", dataModel, viewModel);``


## Models
Models are available on `src/abs/models`.
To add new controller, please do the followings:

1. Define a module name
2. Import the required library
3. Export the module (so controllers could use it)
4. Define an interface
5. Define an implementation class. 


** Any file in `src/abs/models/*.abs` will be red by the compiler for generating a mysql schema. Specifically it does the followings:
  - Create table with table name generated from 'class *Impl' 
  - create table with column generated from 'class *Impl > instance variables' 
  - instance variable with annotation `[PK]` will be primary key

## Views
View files available on `src/abs/views`. View source codes are defined by using HTML.
This file could be written from scratch or be retrieved using UI generator based on IFML.

## Foreign Language Interface
FLI allow developers to call plain java files from ABS. For utilizing this feature, do the followings: 

1. Create the ABS file
  - Define a module name
      - example: `module ABS.Framework.Hello;`
  - Define an interface
  - Define an implementation class that implement the interface
  - Add annotation `[Foreign]` in the implementation class
  - Define the methods that would be implemented by java
      - example: `String hello(String msg) { return "default implementation"; }`
2. Create java file in `src/manual_fli`
  - Define a package whose name equal to the module name in the ABS file
      - example: `package ABS.Framework.Hello;`
  - Define a class whose name same as the corresponding ABS file and add postfix "_fli" to the class name
  - Inherit the previously defined class with ABS class name postfixed by "_c" 
      - example: `public class HelloImpl_fli extends HelloImpl_c {`
  - Declare the methods in ABS file 
  - Convert each ABS class into its java form
      - `String` => `abs.backend.java.lib.types.ABSString`
      - `Int` => `abs.backend.java.lib.types.ABSInteger`
      - `List` => `ABS.StdLib.List`
      - `Pair` => `ABS.StdLib.Pair`
      - ...

** Make sure java class has signature same as abs class

** To know what signature to put in the java file, try to build it first, then look at `target` folder, then examine the signature

## Naming Convention

- Controller Filename => [ClassName]Controller.abs
- Controller Module => M[ClassName]Controller
- Controller Interface => [ClassName]Controller
- Controller Implementation => [ClassName]ControllerImpl


- Model Filename => [ClassName].abs
- Model Module => M[ClassName]Model
- Model Interface => [ClassName]
- Model Implementation => [ClassName]Impl


- Delta Filename => D[DeltaDescription].abs

## Feature Model

Feature Model are available on `src/abs/framework/FeatureModel.abs`.
Feature Model define what product (combination of feature) is valid.

Syntax:
```
root [ProductLineName:String] {
    [VariationPoint]
    ....
    [VariationPoint]
}
```

Variation Point:
```
group <allof, oneof, [N..N]> {
    [FeatureCategory] || [FeatureName]
    ....
    [FeatureCategory] || [FeatureName]
}
```

FeatureCategory:
```
[FeatureCategoryName:String] {
    [VariationPoint]
    ....
    [VariationPoint]
},
```

FeatureName:
```
<empty string/ opt> [FeatureName:String] [Constraint]
```

Constraint: 
```
{<require/exclude>: [FeatureName:String]}
```

Example:
```
root CharityOrganizationSystem {
    group allof {
        PublicationSystem {
            group allof {
                StoryBoard,
                opt MemberNotification {require: Donor;},
                opt Email {require: MemberNotification;}
            }
        },
        FinancialReport {
            group allof {
                Donor,
                opt Summary {exclude: AutomaticReport;},
                opt AutomaticReport 
            }
        },
        opt DonationData {
             group [0..*] {
                Money,
                Item,
                Confirmation
             }
        } ,
   }
}
```

`todo: explain what each keyword mean`

## Feature Model - Basic Tutorial


`opt` only works if the `VariationName`'s quantifier is `allof`.
Other than that(`oneof`, `[N..N]`) ommit the `opt`.

if you want to make a feature that is fully optional use quantifier `[0..*]`, 
And don't use any `opt` in the direct child, example:

```
group [0..*] {
  Money,
  Item,
  Confirmation
}
```

## Feature Grouping Tutorial
Define a folder
```
{"title": "Automatic Report", "key": "Optional", "expand": true, "isFolder": true, "children": [
  ...children goes here...
]}
```
Define a feature. `realName` must follow the syntax in `FeatureModel.abs`
```
{"title": "Email (require:Member Notification)", "key": "[child] Email (require: MemberNotification)", "realName": "Email", "require": "MemberNotification"}
```

## Creating New Menu by Delta
1. in `src/abs/controller`, do below changes:
```

import ViewHelperController, ViewHelperControllerImpl from MViewHelperController;
// interface, return triple
  ... 
  Triple<String, List<Confirmation>, List<Pair<String,Int>>> list(ABSHttpRequest request);
  ...
// impl
  Triple<String, List<Confirmation>, List<Pair<String,Int>>> list(ABSHttpRequest request) {
  ...
    ViewHelperController v = new local ViewHelperControllerImpl();
    List<Pair<String,Int>> viewModel = v.getViewModel();
    return Triple("confirmation/list", continuousPrograms, viewModel);
  ...
  }
```
3. in `src/abs/controller/ViewHelperController`, add new method for the menu, and update method `getViewModel()`
```
// in impl
  Int your_menu(){
    return 0; // zero means, the menu will be hidden
  }
// in impl, getViewModel
  ...
  Int your_menu = this.your_menu();
  ...
  result = appendright(result, Pair("your_menu",your_menu));
  ...
```
4. define delta at `src/abs/delta/DViewHelper.abs`
```
delta DMenuYourMenu;
uses MViewHelperController;
modifies class ViewHelperControllerImpl {
    modifies Int your_menu(){
        return 1;
    }
}
```
5. update `src/abs/framework/ProductLineConfig.abs`
```
delta DMenuYourMenu when YourMenu;
```

## Product Line Configurations

This file define the connection between features (in feature model) and deltas.

syntax:
```
delta TheDelta when <The Feature> || .... ;
```
or 
```
delta TheDelta(Param.param) when <The Feature> && .... ;
```


# Architectures

## Feature Grouping Page
Feature grouping page is the page where user select their feature, this will trigger the build process.
The architecture can be seen at below figure:
![Feature Grouping Architecture](docs/fig-framework-detailed-flow-frontend.png)

## Build Process
### Overview
![Overview](docs/fig-build-process-overview.png)
### Detailed
![Detailed](docs/fig-build-process-detailed.png)

## Message flow
![Message flow](docs/fig-framework-detailed-flow-backend.png)

## Terminology tree
![Terminology tree](docs/fig-knowledge-abs-mvc.png)

## RSE Server Arch (relevant only for RSE lab member)
![RSE Server Arch](docs/fig-rse-server-arch-newarch.png)


# Misc

`generated_products/[ProductName]/probing-status.txt`: contain information 'is the product already accessible on server'.
The reason of creating this file is because when apache detect new reverse proxy configuration,
it needs time to make the internet acknowledge it, so we need a file to show its state.

# Advanced Knowledge

To deal with ORM, edit: `<project_root>/src/orm/AbsDbOrm.java`

# Common Issue

#### 1. Cannot assign UnionType{ } to type ViewHelperController.
> Please make sure to add implement interface, ABS always need an interface
#### 2. Unknown interface or data type / Fail importing modules
> Please make sure to export the modules
#### 3. Method cannot be found
> Please make sure to define an interface
#### 4. No viable alternative input
> ABS cannot perform nested expression "something(something())", please refactor the code
#### 5. How to achieve total optional in feature model?
> define group [0..*], define child without `opt`
#### 6. How to define generic parameter methods?
> To the extent of our knowledge, the ABS compiler which is employed in this framework can not do that
#### 7. Cannot call last delta-ed function
> To the extent of our knowledge, the ABS compiler which is employed in this framework can not do that, calling original() will refer to the original implementation, not the delta-ed one
#### 8. NoClassDeffFoundError: com/fmse/abs/orm/AbsDbOrm
this problem caused by no lib present in built product
> most likely due to build script fail to move library to the built product
> solution: check permission on lib/ and generated_products/
> solution 2: manually copy generated_products/[ProductName]/lib/lib/absdborm.jar to generated_products/[ProductName]/lib/
#### 9. when insert new data not work, no error shown
> Check the database is it pointing the corect one
#### 10. Create data, datanya ga masuk, coba liat di message pas run java, apakah ada "No suitable driver found for nullnull?allowMultiQueries=true"?
> check the permission of config.properties
> you forget to copy auth.properties / config.properties to its designated folder
#### 11. Docker randomly stopped
>  rc local, sysmtem-d : autostart docker
#### 12. "login template blablabla" when logging in into the generated product
> you forget to copy auth.properties / config.properties to its designated folder


#### **If you found another bug, please let us know

# About

ABS MVC Framework is a project that are still maintained by RSE Lab members (see [contributor](#contributor))

For Latest update see [changelog](CHANGELOG.md)

### Contributor

These are the people who was and still contribute (technically or academically) to this project.

| Name            | Contribution            |
| --------------- | ---------------------   |
| Ade Azurat      | Architect & Manager     |
| Maya RAS        | Vice Manager            |
| Salman          | Core Framework          |
| Afifun          | Microservice            |
| Kandito         | ORM                     |
| Niken           | SQL Evolution           |
| Ina Sakinah     | UI Generator using IFML |
| Hafiyyan        | UI Generator using IFML |
| Fakhri          | No-SQL Evolution        |
| Reza Mauliadi   | Feature grouping        |
| Triando         |                         |
| Daya            |                         |
| Alief Aziz      | API Adaptor             |
| Irfan           |                         |
| Ricky Timothy   | Testing                 |
| Rangga          |                         |
| Bethari         | Financial Report        |
| Kenny           | CSS Generator           |
| Affan           | IFML with react         |
| Oman            | SCM                     |
| ......          |                         |

** If you are interested for contributing in this project please contact `ade@cs.ui.ac.id`

# Demo

Here we provide two product samples in AISCO, namely, basic product and advance product.

1. The basic product only contain mandatory features. The followings are information required to enter the page:
    - Product url http://basic.aiscoweb.org
    - Credentials:
        - id : `basic@aiscoweb.org`
        - password : `admint123`
2. The advance product contain all features. The followings are information required to enter the page:
    - Product url http://advance.aiscoweb.org
    - Credentials:
        - id : `basic@aiscoweb.org`
        - password : `admint123`

# Links

[About ABS (Official)](https://abs-models.org/)

[About ABS from RSE UI Lab (In Indonesia)](http://rse.cs.ui.ac.id/?open=event/abs/2017-material)

[Old Github](https://github.com/sir-muamua/ABSMVCFramework)

[Old readme](old-readme.md)
